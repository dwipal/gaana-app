const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')

// Open external urls in default browser
const open = function (target, appName, callback) {
  var opener;

  if (typeof(appName) === 'function') {
    callback = appName;
    appName = null;
  }

  if (appName)
    opener = 'open -a "' + escape(appName) + '"';
  else
    opener = 'open';

  if (process.env.SUDO_USER)
    opener = 'sudo -u ' + process.env.SUDO_USER + ' ' + opener;

  return exec(opener + ' "' + escape(target) + '"', callback);
}

function escape(s) {
  return s.replace(/"/g, '\\\"');
}

// Module to bind media shortcuts
const globalShortcut = require('electron').globalShortcut

// Works only for OS X now
if (process.platform != 'darwin') {
  dialog.showMessageBox({
    "type": "error",
    "title": "Unsupported Platform",
    "message": "Only for Mac"
  });
}



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    //	logo: path.join(__dirname, 'logo.png'),
    webPreferences: {
      plugins: true,
      sandbox: true,
      nodeIntegration: false,
      preload:path.join(__dirname, 'injector.js')
    }
  })

  // And load gaana.com
  mainWindow.loadURL(`https://gaana.com/`)

  // Open DevTools
  //mainWindow.webContents.openDevTools()

  var webView = mainWindow.webContents;

  // Navigation restricted to mainWindow only
  var handleUrl = function (e, url) {
    e.preventDefault()

    // Opening gaaan urls
    if (url.replace('https://', '').replace('http://', '').indexOf('gaana') >= 0)
      mainWindow.loadURL(url)
    else
      // Open External URLs in the default web browser
      open(url)
  }
  webView.on('will-navigate', handleUrl)

  // Bind Media Shortcuts - This wouldn't have been required if the
  // client side code listened to these keys along with KEY_NEXT, KEY_SPACE etc.
  // For now trigger functionality by remapping the keys
  globalShortcut.register('MediaPlayPause', function () {
    webView.send("play-pause-pressed");
  })

  globalShortcut.register('MediaStop', function () {
    webView.send("pause-pressed");
  })
  globalShortcut.register('MediaNextTrack', function () {
    webView.send("next-pressed");
  })

  globalShortcut.register('MediaPreviousTrack', function () {
    webView.send("prev-pressed");
  })

  globalShortcut.register("CommandOrControl+Shift+'", function () {
    webView.send("fav-pressed");
  })
  globalShortcut.register("CommandOrControl+Option+'", function () {
    webView.send("fav-pressed");
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    console.log("mainwindow closed");
    mainWindow = null
  })

  // hide the window instead of closing when `⌘ + W` is used
  mainWindow.on('close', function (e) {
    console.log("mainwindow close");
    if (!mainWindow.forceClose) {
      e.preventDefault();
      mainWindow.hide();
      console.log("prevent default");
    } else {
      console.log("forceclose");
      webView.send("pause-pressed");
      mainWindow.hide();
    }
  });


  // pause music when system is in suspend
  electron.powerMonitor.on('suspend', () => {
     webView.send("auto-pause");
  })

  electron.powerMonitor.on('resume', () => {
     webView.send("auto-play");
  })


  setTimeout(function() {
    webView.send("player-loaded");
    webView.send("play-pressed");

    setTimeout(function() {
      webView.webContents.executeJavaScript("staticPlayer.uiCtrl.updateVolumeSeek(0.2);");
    }, 2000);

  }, 5000);
}




// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  console.log("mainwindow window-all-closed");
  globalShortcut.unregisterAll()
  app.quit()
})

app.on('before-quit', function () {
  console.log("mainwindow before-quit");
  mainWindow.forceClose = true;
});

app.on('activate', function () {
  mainWindow.show();
});



// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
