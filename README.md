# Gaana for Mac
## Download compiled app here:
https://bitbucket.org/dwipal/gaana-app/downloads/Gaana.zip


## Key features:

- Shortcut / support for media keys (play / pause / next / previous / stop)
- Shortcut to add song to favorite - Command+Option+' or Command+Shift+'
- Shows notification when song changes


## Development

Run `npm run` to see all commands defined in `package.json`.

#### Build Locally
```
npm run clean
npm run pack
```

## Credits

Based on Electron, built for Mac OS.
Forked from - [scriptspry.com/2016/07/30/saavn-mac-app.html](http://scriptspry.com/2016/07/30/saavn-mac-app.html).


