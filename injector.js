// injector.js// Get the ipcRenderer of electron
const {ipcRenderer} = require('electron');


ipcRenderer.on("play-pause-pressed",function(event,data){
    $('a.playPause')[0].click();
    show_play_pause_status();
});

function show_play_pause_status() {
    if(title === "") {
        title = "Loading next song.";
    }
    if(is_playing()) {
        show_notification("Play - " + title, album, img);
    } else {
        show_notification("Pause - " + title, album, img);
    }
}

function is_playing() {
    let play_classes=$(".play-song").attr("class").split(' ');
    if($.inArray("play", play_classes) > 0) {
        return false;
    }
    return true;
}

ipcRenderer.on("next-pressed",function(event,data){
    $("a.next-song.next").click();
});
ipcRenderer.on("prev-pressed",function(event,data){
    $("a.prev-song.previous").click();
});
ipcRenderer.on("fav-pressed",function(event,data){
    //console.log($(".event-notification"));
    $("#favorite").click();
    //console.log($(".event-notification"));
    setTimeout(function() {
        show_notification($(".event-notification").text(), "", img);
    }, 1500);
});

var play_on_resume = false;
ipcRenderer.on("auto-pause",function(event,data){
    if(is_playing()) {
        play_on_resume = true;
        $(".play-song.playPause.pause").click();
    }
});

ipcRenderer.on("auto-play",function(event,data){
    if(play_on_resume) {
        $(".play-song.playPause.play").click();
        show_play_pause_status();
        play_on_resume = false;
    }
});

ipcRenderer.on("pause-pressed",function(event,data){
    $(".play-song.playPause.pause").click();
    show_play_pause_status();
});
ipcRenderer.on("play-pressed",function(event,data){
    $(".play-song.playPause.play").click();
    show_play_pause_status();
});


ipcRenderer.on("song-changed",function(event,data){
    song_changed();
});

function show_notification(title, body, icon) {
    let n = new Notification(title, {
      body: body,
      icon: icon,
      silent:true
    });
    setTimeout(n.close.bind(n), 3000);
}

var title="";
var album="";
var img="";

function song_changed() {
    let prev_title = title;

    title=$("#stitle").text();
    album=$("#atitle").text();
    img=$(".player-artwork").find('img').attr('src');

    console.log("Song changed: " + title + " - " + album + ".");

    if(prev_title != title) {
        show_notification(title, album, img);
    } else {
        console.log("previous title is same as new one, not showing song change notification.");
    }

}

ipcRenderer.on("player-loaded",function(event,data){
    console.log("Player loaded");

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if(mutation.target.classList.contains("thumbHolder")) {
                song_changed();
            } 
        });
    });
    var config = { attributes: true, childList: true, characterData: true }
    observer.observe(document.querySelector('.thumbHolder'), config);

    // Update the volume
    //staticPlayer.uiCtrl.updateVolumeSeek(0.2);
});

console.log("Injector injected.");
